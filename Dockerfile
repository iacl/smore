FROM pytorch/pytorch:2.0.0-cuda11.7-cudnn8-runtime
RUN apt-get update && apt-get install -y --no-install-recommends git

COPY . /tmp/smore

RUN pip install /tmp/smore && rm -rf /tmp/smore

ENTRYPOINT ["run-smore"]
