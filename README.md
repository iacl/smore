# SMORE: Synthetic Multi-Orientation Resolution Enhancement | [Paper](https://link.springer.com/chapter/10.1007/978-3-031-44689-4_12) | [Paper](https://doi.org/10.1109/TMI.2020.3037187)
SMORE is a self super-resolution method; no outside training data is required.
The only required input is the image itself and the output directory.

This specific code version is associated with [Remedios et al. 2023](https://link.springer.com/chapter/10.1007/978-3-031-44689-4_12).

## Installation from Source

### Install using `pip`
There is no need to clone this repository. Just run:
 ```pip install git+https://gitlab.com/iacl/smore@v4.0.5```

Package requirements are automatically handled. To see a list of requirements, see `setup.py` L50-59.
This installs the `smore` package and creates CLI aliases `run-smore`, `smore-train`, and `smore-test`.
 
### Basic Usage

There are two basic ways to run this code: from source after running `pip install .` or from the Singularity container.

```run-smore --in-fpath ${INPUT_FPATH} --out-dir ${OUTPUT_DIR} [--slice-thickness ${SLICE_THICKNESS}] [--blur-kernel-file ${BLUR_KERNEL_FILE}] [--gpu-id ${GPU_ID}] [--suffix ${SUFFIX}]```

or, via the Singularity image:

```singularity run --nv -B ${DRIVE_ROOT} /path/to/container/smore-4.0.0-rc.sif --in-fpath ${INPUT_FPATH} --out-dir ${OUTPUT_DIR} [--slice-thickness ${SLICE_THICKNESS}] [--blur-kernel-file ${BLUR_KERNEL_FILE}] [--gpu-id ${GPU_ID}] [--suffix ${SUFFIX}]```

where each argument in `[]` is optional and may be omitted. Remember with Singularity that you need to bind the right drives with `-B ${DRIVE_ROOT}` where `${DRIVE_ROOT}` is where your data sits. The default GPU ID is `0`. The container will automatically handle whether the slice thickness or blur kernel file is present, or neither case. The `--suffix` argument allows you to specify the suffix of the output filename before the file extension. Default is `_smore4`.

## Citations
If this work is useful to you or your project, please consider citing our relevant papers:

```
Remedios, S.W., Han, S., Zuo, L., Carass, A., Pham, D.L., Prince, J.L. 
and Dewey, B.E., 2023, October. Self-supervised super-resolution for 
anisotropic MR images with and without slice gap. In International 
Workshop on Simulation and Synthesis in Medical Imaging (pp. 118-128). 
Cham: Springer Nature Switzerland.
```

```
C. Zhao, B. E. Dewey, D. L. Pham, P. A. Calabresi, D. S. Reich and J. L. Prince,
"SMORE: A Self-Supervised Anti-Aliasing and Super-Resolution Algorithm for MRI 
Using Deep Learning," in IEEE Transactions on Medical Imaging, vol. 40, no. 3, 
pp. 805-817, March 2021, doi: 10.1109/TMI.2020.3037187.
```

```
C. Zhao, M. Shao, A. Carass, H. Li, B. E. Dewey, L. M. Ellingsen, J. Woo, 
M. A. Guttman, A. M. Blitz, M. Stone, P. A. Calabresi, H. Halperin, and J. L. Prince, 
“Applications of a deep learning method for anti-aliasing and super-resolution in 
MRI”, Magnetic Resonance Imaging, 64:132-141, 2019.
```

```
Zhao C., Son S., Kim Y., Prince J.L. (2019) iSMORE: An Iterative Self 
Super-Resolution Algorithm. In: Burgos N., Gooya A., Svoboda D. (eds) Simulation 
and Synthesis in Medical Imaging. SASHIMI 2019. Lecture Notes in Computer Science, 
vol 11827. Springer, Cham. https://doi.org/10.1007/978-3-030-32778-1_14
```

```
Zhao C. et al. (2018) A Deep Learning Based Anti-aliasing Self Super-Resolution 
Algorithm for MRI. In: Frangi A., Schnabel J., Davatzikos C., Alberola-López C., 
Fichtinger G. (eds) Medical Image Computing and Computer Assisted Intervention – 
MICCAI 2018. MICCAI 2018. Lecture Notes in Computer Science, vol 11070. Springer, 
Cham. https://doi.org/10.1007/978-3-030-00928-1_12
```

```
C. Zhao, A. Carass, B. E. Dewey and J. L. Prince, "Self super-resolution for
magnetic resonance images using deep networks," 2018 IEEE 15th International 
Symposium on Biomedical Imaging (ISBI 2018), 2018, pp. 365-368, 
doi: 10.1109/ISBI.2018.8363594.
```
